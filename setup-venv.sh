#!/bin/sh

if [ "$#" -lt 1 ]; then
    echo "Expected 1 argument, got $#" >&2
    exit 1
fi

export PATH="$1/bin:$PATH"

