#!/usr/bin/python

import aiohttp
import discord
from discord.ext import commands

import asyncio
import os
import sys


def load_token():
    if len(sys.argv) <= 1:
        return load_token_from_file("password.txt")
    else:
        return load_token_from_file(sys.argv[1])


def load_token_from_file(path):
    with open(path, "r") as inFile:
        return inFile.read().strip()


try:
    token = load_token()
except Exception as err:
    sys.stderr.write("Couldn't load the token :(\n")
    sys.stderr.write(str(err))
    sys.stderr.write("\n")
    exit(1)

print("[INFO] Logging in with token {}".format(token))


intents = discord.Intents.default()
intents.message_content = True
bot = commands.Bot(command_prefix = "dns!", intents=intents)

@bot.event
async def on_ready():
    print("Connected to", bot.user.name)


@bot.command(name = "ip")
async def cmd_ip(ctx):
    # I don't use aiohttp's caching because I don't want changes of IP to take
    # the bot down for too long or to give incorrect results
    async with aiohttp.ClientSession() as session:
        async with session.get("http://ipv4.icanhazip.com") as response:
            await ctx.send(await response.text())


bot.run(token)

